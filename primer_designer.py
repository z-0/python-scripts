# -*- coding: utf-8 -*-

import os
import prettytable
import sys
from termcolor import colored as coloured
from termcolor import cprint


def complement(seq):
    complement = ""

    for base in seq.upper():
        if base == "G":
            complement += "C"
        elif base == "C":
            complement += "G"

        elif base == "A":
            complement += "T"
        elif base == "T":
            complement += "A"

    return complement


def count(seq):
    g, c, a, t = 0, 0, 0, 0

    for base in seq.upper():

        if base == "G":
            g += 1
        elif base == "C":
            c += 1

        elif base == "A":
            a += 1
        elif base == "T":
            t += 1
    return g, c, a, t


def calc_Tm(primer_seq):
    g, c, a, t = count(primer_seq)

    Tm = 4 * (g + c) + 2 * (a + t)

    return Tm


def generate_primers(n, seq):
    primers = []
    pos = 0

    while True:

        new_seq = seq[pos:n + pos]

        pos += 1
        length = len(new_seq)

        if length != n:
            break

        primers.append(new_seq)
    return primers


def gen_primer_packs(primers):
    pTms = []

    for primer in primers:
        Tm = calc_Tm(primer)

        pTm = {}
        pTm['Primer'] = primer
        pTm['Tm'] = Tm

        pTms.append(pTm)

    return pTms

def find_Tm_primers(primers, between):
    pTms = gen_primer_packs(primers)
    found = []

    for pTm in pTms:

        Tm = pTm['Tm']

        if Tm >= between[0] and Tm <= between[1]:
            found.append(pTm)

    return found


def match_primer_tms(fpTms, rpTms):
    matches = []
    pos = 1
    for fprimer, rprimer in zip(fpTms, rpTms):

        if fprimer['Tm'] == rprimer['Tm']:
            match = {}
            match['F'] = fprimer
            match['R'] = rprimer
            match['Pos'] = pos
            matches.append(match)
        pos += 1
    return matches


if __name__ == "__main__":
    os.system("cls")
    dg = u'\N{DEGREE SIGN}'

    try:
        filename = sys.argv[4]
        sense = open(filename, "r").read().upper()
    except IndexError:

        sense = "atgtaccgtaggctaagctagcggtagcgtagcttgcatgctaggctcgatatagatctgccc".upper()

    antisense = complement(sense)
    print "5'-%s-3'" % sense
    print "3'-%s-5'" % antisense
    print
    plen = int(sys.argv[1])
    bottom = int(sys.argv[2])
    top = int(sys.argv[3])

    fprimers = generate_primers(plen, sense)
    bprimers = generate_primers(plen, antisense[::-1])

    a_fprimers = find_Tm_primers(fprimers, (bottom, top))
    a_rprimers = find_Tm_primers(bprimers, (bottom, top))

    pt = prettytable.PrettyTable(["Direction", "Pos", "Sequence", "Tm"])
    print "Primer length: %d nucleotides" % plen 
    print "Tm range: %d%sC - %d%sC (Inclusive)" % (bottom, dg, top, dg)
    print
    for idx, pTm in enumerate(a_fprimers):
        pt.add_row(["Forward", idx + 1, pTm['Primer'], "%d%sC" % (pTm['Tm'], dg)])

    pt.add_row(["-", "-", "-", "-"])

    for idx, pTm in enumerate(a_rprimers):
        pt.add_row(["Reverse", idx + 1, pTm['Primer'], "%d%sC" % (pTm['Tm'], dg)])
    print pt.get_string()
    """
    matches = match_primer_tms(a_fprimers, a_rprimers)

    pt1 = prettytable.PrettyTable(["Pos", "Forward", "Reverse", "Tm"])
    for match in matches:
        pt1.add_row([match['Pos'], match['F']['Primer'], match['R']['Primer'], match['F']['Tm']])

    print pt1.get_string()
    """
