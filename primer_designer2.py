# -*- encoding: utf-8 -*-

import os
import sys
import prettytable


class Sequence(object):
    def __init__(self, seq):
        self.sequence = seq


    def count(self):
        g, c, a, t = 0, 0, 0, 0

        for base in self.sequence.upper():

            if base == "G":
                g += 1
            elif base == "C":
                c += 1

            elif base == "A":
                a += 1
            elif base == "T":
                t += 1
        return g, c, a, t

class DNASequence(Sequence):
    def __init__(self, seq):
        Sequence.__init__(self, seq)
        self.sequence = seq.upper()
        self.complement = self.synth_complement()
        self.reverse_complement = self.synth_complement()[::-1]

    def synth_complement(self):
        pairs = [("G", "C"), ("A", "T")]
        comp_seq = ""

        for base in self.sequence:

            for pair in pairs:
                if base == pair[0]:
                    comp_seq += pair[1]
                elif base == pair[1]:
                    comp_seq += pair[0]

        return comp_seq


class Primer(Sequence):
    def __init__(self, seq):
        Sequence.__init__(self, seq)
        self.sequence = seq
        self.Tm = self.calc_Tm()

    def calc_Tm(self):
        g, c, a, t = self.count()

        Tm = 4 * (g + c) + 2 * (a + t)

        return Tm

def generate_primers(dna, length):

    forward_primers = []
    reverse_primers = []
    pos = 0

    for pos in xrange(len(dna.sequence)):

        primerseq = dna.sequence[pos:pos + length]

        if len(primerseq) != length:
            break

        P = Primer(primerseq)

        forward_primers.append(P)

        pos += 1

    for pos in xrange(len(dna.sequence)):

        primerseq = dna.reverse_complement[pos:pos + length]

        if len(primerseq) != length:
            break

        P = Primer(primerseq)

        reverse_primers.append(P)

    return forward_primers, reverse_primers

def filter_primers(primers, minimum, maximum):

    filtered_primers = []

    for primer in primers:
        if primer.Tm >= minimum and primer.Tm <= maximum:
            filtered_primers.append(primer)

    return filtered_primers

def nice_display(DNA):

    dg = u'\N{DEGREE SIGN}'

    os.system("cls")
    print "5'-%s-3'" % DNA.sequence
    print "3'-%s-5'" % DNA.complement
    print
    length = int(raw_input("Primer Length: "))
    print
    filter = raw_input("Filter Primers? [Y]: ")

    fprimers, rprimers = generate_primers(DNA, length)
    PT = prettytable.PrettyTable(["Pos", "F Sequence", "F Tm", "R Sequence", "R Tm"])
    if filter.upper() == "Y":
        print
        print "Range is inclusive!"
        print
        minimum = int(raw_input("Minimum Tm: "))
        maximum = int(raw_input("Maximum Tm: "))
        print
        fprimers, rprimers = filter_primers(fprimers, minimum, maximum), filter_primers(rprimers, minimum, maximum)
    else:
        print
    count = 1

    for fprimer, rprimer in zip(fprimers, rprimers):

        fseq, fTm = fprimer.sequence, "%d%sC" % (fprimer.Tm, dg)
        rseq, rTm = rprimer.sequence, "%d%sC" % (rprimer.Tm, dg)
        pos = str(count)
        PT.add_row([pos, fseq, fTm, rseq, rTm])
        count += 1

    os.system("cls")
    print "5'-%s-3'" % DNA.sequence
    print "3'-%s-5'" % DNA.complement
    print
    print PT.get_string()


if __name__ == "__main__":


    try:
        seq = open(sys.argv[1], "r").read().strip("\n")
    except IndexError:
        seq = "atgtaccgtaggctaagctagcggtagcgtagcttgcatgctaggctcgatatagatctgccc"
    DNA = DNASequence(seq)
    f, r = generate_primers(DNA, 20)

    f = filter_primers(f, 55, 60)
    r = filter_primers(r, 55, 60)

    nice_display(DNA)